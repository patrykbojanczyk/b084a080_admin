function Task(task) {
    this.id = task.id;
    this.title = task.title;
    this.description = task.description;
    this.coordinates = task.coordinates;
    this.question = task.question;
    this.answer = task.answer;
    this.nextTaskId = task.nextTaskId;
}