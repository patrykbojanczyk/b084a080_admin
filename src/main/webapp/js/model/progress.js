function Progress(progress) {
    this.id = progress.id;
    this.scenarioId = progress.scenarioId;
    this.userId = progress.userId;
    this.currentTaskId = progress.currentTaskId;
}