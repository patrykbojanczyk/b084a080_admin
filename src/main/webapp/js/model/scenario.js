function Scenario(scenario) {
    this.id = scenario.id;
    this.title = scenario.title;
    this.description = scenario.description;
    this.authorId = scenario.authorId;
    this.firstTaskId = scenario.firstTaskId;
}