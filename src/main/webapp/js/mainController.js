var mainApp = angular.module("theWalkingGame", [ 'ngRoute', 'ngResource', 'uiGmapgoogle-maps',
    'ui', 'ui.bootstrap' ]);

mainApp.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            controller: "mainPageController",
            templateUrl: "struct/page/main.html"
        })
        .when("/scenario/all/:userId", {
            controller: "scenarioAllController",
            templateUrl: "struct/page/scenario/all.html"
        })
        .when("/scenario/edit/:scenarioId", {
            controller: "scenarioEditController",
            templateUrl: "struct/page/scenario/edit.html"
        })
        .when("/scenario/info/:scenarioId", {
            controller: "scenarioInfoController",
            templateUrl: "struct/page/scenario/info.html"
        })
        .when("/scenario/add", {
            controller: "scenarioAddController",
            templateUrl: "struct/page/scenario/add.html"
        })
        .when("/task/info/:scenarioId/:taskOrder", {
            controller: "taskInfoController",
            templateUrl: "struct/page/task/info.html"
        })
        .when("/task/edit/:scenarioId/:taskOrder", {
            controller: "taskEditController",
            templateUrl: "struct/page/task/edit.html"
        })
        .when("/task/add/:scenarioId", {
            controller: "taskAddController",
            templateUrl: "struct/page/task/add.html"
        })
        .otherwise({
            redirectTo: "/"
        });
});

mainApp.controller("headerController", function($scope) {
    $scope.userId = "1aab95e2335b40eeb5cf07a6e1c691a8";
});

mainApp.controller("mainPageController", function($scope) {
});


//SCENARIO
mainApp.controller("scenarioAllController", function($scope, $routeParams, $http) {
    $http
        .get("/rest/scenario/all", {
            params: {
                userId: $routeParams.userId
            }
        })
        .then(function(response) {
            $scope.scenarios = angular.fromJson(response.data);
        }, function() {
            //error handler
        });
});
mainApp.controller("scenarioInfoController", function($scope, $routeParams, $http, $window) {
    $scope.authorId = "1aab95e2335b40eeb5cf07a6e1c691a8";
    getScenarioFullInfo($scope, $routeParams, $http);
    $scope.backButton = function() {
        $window.location.href = '#/scenario/all/'+$scope.authorId;
    }
});
mainApp.controller("scenarioAddController", function($scope, $http, $window) {
    $scope.authorId = "1aab95e2335b40eeb5cf07a6e1c691a8";
    $scope.addScenarioSubmit = function() {
        $http
            .post("/rest/scenario/add", {
                id: "",
                authorId: $scope.authorId,
                firstTaskId: null,
                title: $scope.scenario.title,
                description: $scope.scenario.description
            }).then(function() {
                $window.location.href = '#/scenario/all/'+$scope.authorId;
            }, function() {
                //error handler
            });
    }
});
mainApp.controller("scenarioEditController", function($scope, $routeParams, $http, $window) {
    $scope.authorId = "1aab95e2335b40eeb5cf07a6e1c691a8";
    getScenarioFullInfo($scope, $routeParams, $http);

    $scope.editScenarioSubmit = function() {
        $http
            .post("/rest/scenario/edit", {
                id: $scope.scenario.id,
                title: $scope.scenario.title,
                description: $scope.scenario.description,
                authorId: $scope.scenario.authorId,
                firstTaskId: $scope.scenario.firstTaskId
            }).then(function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            }, function() {
                //error handler
            });
    };

    $scope.editScenarioTasksSubmit = function() {
        $http
            .post("/rest/scenario/edit/tasks", {
                id: $scope.scenario.id,
                tasks: $scope.tasks
            }).then(function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            }, function() {
                //error handler
            });
    };

    $scope.removeScenarioSubmit = function(id) {
        $http
            .post("/rest/scenario/remove/"+id, {})
            .then(function() {
                $window.location.href = '#/scenario/all/'+$scope.authorId;
            }, function() {
                //error handler
            });
    }

    $scope.deleteTask = function(index) {
        $scope.tasks.splice(index, 1);
    };

    $scope.backButton = function() {
        goBack();
    }
});

mainApp.controller("taskInfoController", function($scope, $routeParams, $http, $window) {
    getTaskFullInfo($scope, $routeParams, $http, false, $window)
    $scope.backButton = function() {
        $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
    }
});
mainApp.controller("taskEditController", function($scope, $routeParams, $http, $window) {
    getTaskFullInfo($scope, $routeParams, $http, true, $window);

    $scope.backButton = function() {
        goBack();
    }

});
mainApp.controller("taskAddController", function($scope, $routeParams, $http, $window) {
    $scope.task = {
        id: "0",
        coordinates: {
            latitude:  53.0071,
            longitude: 18.601
        },
        nextTaskId: null
    };

    prepareMap($scope, true);

    $scope.taskAddSubmit = function() {

        $http
            .post("/rest/task/add", {
                scenarioId: $routeParams.scenarioId,
                task: $scope.task
            }).then(function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            }, function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            });
    };

    $scope.backButton = function() {
        goBack();
    }

});


function getTaskFullInfo($scope, $routeParams, $http, draggable, $window) {
    $scope.scenarioId = $routeParams.scenarioId;
    $scope.taskOrder = $routeParams.taskOrder;
    $http
        .get("/rest/task/get", {
            params: {
                scenarioId: $routeParams.scenarioId,
                taskNum: $routeParams.taskOrder
            }
        }).then(function(response) {
            $scope.task = response.data;
            prepareMap($scope, draggable)
        });

    $scope.editTaskSubmit = function() {
        $http
            .post("/rest/task/edit/"+$routeParams.scenarioId+"/"+$routeParams.taskOrder, {
                id: $scope.task.id,
                title: $scope.task.title,
                description: $scope.task.description,
                coordinates: {
                    latitude: $scope.task.coordinates.latitude,
                    longitude: $scope.task.coordinates.longitude
                },
                question: $scope.task.question,
                answer: $scope.task.answer,
                nextTaskId: $scope.task.nextTaskId
            }).then(function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            }, function() {
                $window.location.href = '#/scenario/info/'+$routeParams.scenarioId;
            });
    }
}

function prepareMap($scope, draggable) {
    $scope.map = {center: {
        latitude: $scope.task.coordinates.latitude,
        longitude: $scope.task.coordinates.longitude
    },
        zoom: 10 };
    $scope.options = {};
    $scope.marker = {
        id: 1,//$scope.task.id,
        options: { draggable: draggable },
        coords: {
            latitude: $scope.task.coordinates.latitude,
            longitude: $scope.task.coordinates.longitude
        },
        events: {
            dragend: function (marker, eventName, args) {
                $scope.task.coordinates.latitude = marker.getPosition().lat();
                $scope.task.coordinates.longitude = marker.getPosition().lng();
            }
        }
    };
}

function getScenarioFullInfo($scope, $routeParams, $http) {
    $http
        .get("/rest/scenario/get", {
            params: {
                scenarioId: $routeParams.scenarioId
            }
        })
        .then(function(response) {
            $scope.scenario = angular.fromJson(response.data);
            $http
                .get("/rest/task/all", {
                    params: {
                        scenarioId: $routeParams.scenarioId
                    }
                }).then(function(response) {
                    $scope.tasks = angular.fromJson(response.data);
                })
        });
}

function goBack() {
    window.history.back();
}