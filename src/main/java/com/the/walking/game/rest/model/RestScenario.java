package com.the.walking.game.rest.model;

import java.util.UUID;

public class RestScenario {
    public String id;
    public String title;
    public String description;
    public String authorId;
    public String firstTaskId;

    public RestScenario() {
        this.id = UUID.randomUUID().toString();
        this.title = UUID.randomUUID().toString();
        this.description = UUID.randomUUID().toString();
        this.authorId = UUID.randomUUID().toString();
        this.firstTaskId = UUID.randomUUID().toString();
    }

    public RestScenario(String id, String title, String description, String authorId, String firstTaskId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.authorId = authorId;
        this.firstTaskId = firstTaskId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getFirstTaskId() {
        return firstTaskId;
    }

    public void setFirstTaskId(String firstTaskId) {
        this.firstTaskId = firstTaskId;
    }
}
