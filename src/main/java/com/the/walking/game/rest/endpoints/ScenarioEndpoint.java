package com.the.walking.game.rest.endpoints;

import com.the.walking.game.rest.model.EditScenarioTasks;
import com.the.walking.game.rest.model.RestScenario;
import com.the.walking.game.rest.model.RestTask;
import com.the.walking.game.rest.services.IScenarionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/rest/scenario")
public class ScenarioEndpoint {
    @Autowired private IScenarionService scenarionService;

    @ResponseBody
    @RequestMapping(
            value = "/get",
            method = RequestMethod.GET
    )
    public RestScenario get(
            @RequestParam(name = "scenarioId") String scenarioId
    ) throws IOException, URISyntaxException {
        return scenarionService.get(scenarioId);
    }

    @ResponseBody
    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET
    )
    public List<RestScenario> getAll(
            @RequestParam(name = "userId") String userId
    ) throws IOException, URISyntaxException {
        return scenarionService.getAll(userId);
    }

    @RequestMapping(
            value = "/edit",
            method = RequestMethod.POST
    )
    public void edit(
            @RequestBody RestScenario scenario
    ) throws IOException {
        scenarionService.edit(scenario);
    }

    @RequestMapping(
            value = "/edit/tasks",
            method = RequestMethod.POST
    )
    public void editScenarioTasks(
            @RequestBody EditScenarioTasks editScenarioTasks
    ) {
        scenarionService.editScenarioTasks(editScenarioTasks.getId(), editScenarioTasks.getTasks());
    }

    @RequestMapping(
            value = "/remove/{scenarioId}",
            method = RequestMethod.POST
    )
    public void removeScenario(
            @PathVariable(value = "scenarioId") String scenarioId
    ) throws IOException {
        scenarionService.remove(scenarioId);
    }

    @ResponseBody
    @RequestMapping(
            value = "/add",
            method = RequestMethod.POST
    )
    public String add(
            @RequestBody RestScenario restScenario
    ) throws IOException {
        return scenarionService.add(restScenario);
    }
}
