package com.the.walking.game.rest.model;


import java.util.List;

public class EditScenarioTasks {
    private String id;
    private List<RestTask> tasks;

    public EditScenarioTasks() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<RestTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<RestTask> tasks) {
        this.tasks = tasks;
    }
}
