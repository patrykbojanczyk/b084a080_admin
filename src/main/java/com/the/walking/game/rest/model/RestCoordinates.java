package com.the.walking.game.rest.model;

import java.util.Random;

public class RestCoordinates {
    private final double latitude;
    private final double longitude;

    public RestCoordinates() {
        this.latitude = random();
        this.longitude = random();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private double random() {
        int rangeMin = -90;
        int rangeMax = 90;

        return rangeMin + (rangeMax - rangeMin) * new Random().nextDouble();
    }
}
