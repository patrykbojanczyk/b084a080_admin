package com.the.walking.game.rest.services;

import com.the.walking.game.rest.model.RestTask;

import java.util.List;

public interface ITaskService extends ICrud<RestTask> {
    public List<RestTask> getAll(String scenarioId);
    public RestTask get(String scenarioId, String taskNum);
    public void edit(String scenarioId, String taskNum, RestTask restTask);
    public String add(String scenarioId, RestTask restTask);
}
