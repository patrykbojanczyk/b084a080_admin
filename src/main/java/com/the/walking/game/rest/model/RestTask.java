package com.the.walking.game.rest.model;

import java.util.UUID;

public class RestTask {
    private String id;
    private String title;
    private String description;
    private RestCoordinates coordinates;
    private String question;
    private String answer;
    private String nextTaskId;

    public RestTask() {
        this.id = UUID.randomUUID().toString();
        this.title = UUID.randomUUID().toString();
        this.description = UUID.randomUUID().toString();
        this.coordinates = new RestCoordinates();
        this.question = null;
        this.answer = null;
        this.nextTaskId = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public RestCoordinates getCoordinates() {
        return coordinates;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getNextTaskId() {
        return nextTaskId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCoordinates(RestCoordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setNextTaskId(String nextTaskId) {
        this.nextTaskId = nextTaskId;
    }
}
