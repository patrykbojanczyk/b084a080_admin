package com.the.walking.game.rest.services;

import com.the.walking.game.rest.model.RestScenario;
import com.the.walking.game.rest.model.RestTask;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface IScenarionService extends ICrud<RestScenario> {
    public List<RestScenario> getAll(String userId) throws IOException, URISyntaxException;
    public void editScenarioTasks(String scenarioId, List<RestTask> tasks);
}
