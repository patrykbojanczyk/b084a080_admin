package com.the.walking.game.rest.services;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface ICrud<T> {
    public T get(String id) throws IOException, URISyntaxException;
    public List<T> getAll() throws IOException, URISyntaxException;
    public String add(T object) throws IOException;
    public void edit(T object) throws IOException;
    public void remove(String object) throws IOException;
}
