package com.the.walking.game.rest.services;

import com.the.walking.game.rest.model.RestScenario;
import com.the.walking.game.rest.model.RestTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public class ScenarionService implements IScenarionService {
    private RestTemplate restTemplate;
    private final String API_URL;
    private final String CLEAR_API_URL;

    @Autowired
    public ScenarionService(
            @Value("${api.url}") String apiUrl
    ) {
        this.API_URL = apiUrl+"/scenario/";
        this.CLEAR_API_URL = apiUrl;
        this.restTemplate = new RestTemplate();
    }


    @Override
    public List<RestScenario> getAll(String userId) {
        ResponseEntity<RestScenario[]> responseEntity
                = restTemplate.getForEntity(API_URL + "GetAllForUser/" + userId, RestScenario[].class);

        return Arrays.asList(responseEntity.getBody());
    }

    @Override
    public void editScenarioTasks(String scenarioId, List<RestTask> tasks) {
        restTemplate.postForObject(CLEAR_API_URL + "/task/modify/" + scenarioId, tasks, String.class);
    }

    @Override
    public RestScenario get(String id) {
        return restTemplate.getForObject(API_URL + "getspecific/" + id, RestScenario.class);
    }

    @Override
    public List<RestScenario> getAll() {
        ResponseEntity<RestScenario[]> responseEntity
                = restTemplate.getForEntity(API_URL + "GetAll", RestScenario[].class);

        return Arrays.asList(responseEntity.getBody());
    }

    @Override
    public String add(RestScenario object) {
        return restTemplate.postForObject(API_URL + "CreateScenario", object, String.class);
    }

    @Override
    public void edit(RestScenario object) {
        restTemplate.postForObject(API_URL + "ModifyScenario/"+object.getId(), object, String.class);
    }

    @Override
    public void remove(String object) {
        restTemplate.postForObject(API_URL + "DeleteScenario/"+object, null, String.class);
    }
}
