package com.the.walking.game.rest.endpoints;

import com.the.walking.game.rest.model.AddTask;
import com.the.walking.game.rest.model.RestTask;
import com.the.walking.game.rest.services.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/rest/task")
public class TaskEndpoint {
    @Autowired private ITaskService taskService;

    @ResponseBody
    @RequestMapping(
            value = "/get",
            method = RequestMethod.GET
    )
    public RestTask get(
            @RequestParam(name = "scenarioId") String scenarioId,
            @RequestParam(name = "taskNum") String taskNum
    ) throws IOException, URISyntaxException {
        return taskService.get(scenarioId, taskNum);
    }

    @ResponseBody
    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET
    )
    public List<RestTask> getAll(
            @RequestParam(name = "scenarioId") String scenarioId
    ) {
        return taskService.getAll(scenarioId);
    }

    @RequestMapping(
            value = "/edit/{scenarioId}/{taskNum}",
            method = RequestMethod.POST
    )
    public void edit(
            @PathVariable(value = "scenarioId") String scenarioId,
            @PathVariable(value = "taskNum") String taskNum,
            @RequestBody RestTask restTask
    ) throws IOException {
        taskService.edit(scenarioId, taskNum, restTask);
    }

    @RequestMapping(
            value = "/add",
            method = RequestMethod.POST
    )
    public boolean add(
            @RequestBody AddTask addTask
    ) {
        taskService.add(addTask.getScenarioId(), addTask.getTask());
        return true;
    }
}
