package com.the.walking.game.rest.services;

import com.the.walking.game.rest.model.RestScenario;
import com.the.walking.game.rest.model.RestTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public class TaskService implements ITaskService {
    private final RestTemplate restTemplate;
    private final String API_URL;

    @Autowired
    public TaskService(
            @Value("${api.url}") String apiUrl
    ) {
        this.API_URL = apiUrl + "/task/";
        this.restTemplate = new RestTemplate();
    }

    @Override
    public List<RestTask> getAll(String scenarioId) {
        ResponseEntity<RestTask[]> responseEntity
                = restTemplate.getForEntity(API_URL + "GetAll/"+scenarioId, RestTask[].class);

        return Arrays.asList(responseEntity.getBody());
    }

    @Override
    public RestTask get(String scenarioId, String taskNum) {
        return restTemplate.getForObject(API_URL + "getspecific/"+scenarioId+"/"+taskNum, RestTask.class);
    }

    @Override
    public void edit(String scenarioId, String taskNum, RestTask restTask) {
        restTemplate.postForObject(API_URL + "modify/"+scenarioId+"/"+taskNum, restTask, String.class);
    }

    @Override
    public String add(String scenarioId, RestTask restTask) {
        return restTemplate.postForObject(API_URL + "add/" + scenarioId, restTask, String.class);
    }

    @Override
    public RestTask get(String id) {
        return null;
    }

    @Override
    public List<RestTask> getAll() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String add(RestTask object) {
        return "";
    }

    @Override
    public void edit(RestTask object) {
    }

    @Override
    public void remove(String object) {

    }
}
