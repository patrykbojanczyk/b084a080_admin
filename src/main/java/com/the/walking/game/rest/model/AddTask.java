package com.the.walking.game.rest.model;

public class AddTask {
    private String scenarioId;
    private RestTask task;

    public AddTask() {
    }

    public String getScenarioId() {
        return scenarioId;
    }

    public void setScenarioId(String scenarioId) {
        this.scenarioId = scenarioId;
    }

    public RestTask getTask() {
        return task;
    }

    public void setTask(RestTask task) {
        this.task = task;
    }
}
